﻿using LNW.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LifeNoteWeb.Models
{
    public class MyCalendarViewModel
    {
        //CONSTRUCTEUR
        public MyCalendarViewModel(DateTime? id, List<ListTask> listLT)
        {
            SelectedDay = new DayTask();
            if (id == null)
                SelectedDay.DayDate = DateTime.Today;
            else
                SelectedDay.DayDate = id.Value;


            _monthNumber = SelectedDay.DayDate.Month;

            _monthName = FindNameMonth(_monthNumber);

            FindNumberOfDaysIntoMonth();

            ThisMonth = new MonthTask();
            FindStartFrom();

            ThisMonth.ListDayTask = new List<DayTask>();
            ThisMonth.ListOfListTask = new List<ListTask>();
            ThisMonth.NumbersOfDay = _numberDayIntoMonth;
            //from all listTask to only of selected month
            foreach (ListTask item in listLT)
            {
                if (item.DateOfList.Month == _monthNumber)
                    if (item.Works.Count > 0)
                        ThisMonth.ListOfListTask.Add(item);
            }
            //foreach dayTask there is a ListTask
            for (int i = 1; i <= _numberDayIntoMonth; i++)
            {
                ThisDay = new DayTask();
                ThisDay.DayNumber = i;
                ThisDay.DayDate = new DateTime(2018, MonthNumber.Value, i);

                foreach (ListTask item in ThisMonth.ListOfListTask)
                {
                    if (item.DateOfList.Day == i)
                    {
                        ThisDay.ListWork = new List<Work>(item.Works);
                        ThisDay.ThereIsWork = true;
                        if (item.DateOfList == SelectedDay.DayDate)
                        {
                            SelectedDay.DayNumber = i;
                            SelectedDay.ListWork = new List<Work>(ThisDay.ListWork);
                            SelectedDay.ThereIsWork = true;
                            ThisDay.DayDate = item.DateOfList;
                        }
                    }
                }
                ThisMonth.ListDayTask.Add(ThisDay);
            }
        }
        //VARIABLES
        private string _monthName;
        private int? _monthNumber;
        private int _numberDayIntoMonth;

        //PROPRIETES
        public string MonthName
        {
            get { return _monthName; }
            set { _monthName = value; }
        }
        public int? MonthNumber
        {
            get { return _monthNumber; }
            set { _monthNumber = value; }
        }
        public int NumberDayIntoMonth
        {
            get { return _numberDayIntoMonth; }
            set { _numberDayIntoMonth = value; }
        }
        public DayTask ThisDay { get; set; } //inside ThisMonth
        public MonthTask ThisMonth { get; set; }
        public DayTask SelectedDay { get; set; }

        #region Helpers
        public string FindNameMonth(int? MonthToSearch)
        {
            string month = "";
            switch (MonthToSearch)
            {
                case 1: month = "January"; break;
                case 2: month = "February"; break;
                case 3: month = "Mars"; break;
                case 4: month = "April"; break;
                case 5: month = "Maj"; break;
                case 6: month = "Juan"; break;
                case 7: month = "Juillet"; break;
                case 8: month = "Aout"; break;
                case 9: month = "Septembre"; break;
                case 10: month = "Octobre"; break;
                case 11: month = "Novembre"; break;
                case 12: month = "Decembre"; break;
                default:
                    break;
            }
            return month;
        }
        public void FindNumberOfDaysIntoMonth()
        {
            if (_monthNumber == 2)
                _numberDayIntoMonth = 28;
            else if (_monthNumber == 4 || _monthNumber == 6 || _monthNumber == 9 || _monthNumber == 11)
                _numberDayIntoMonth = 30;
            else
                _numberDayIntoMonth = 31;
        }
        public void FindStartFrom()
        {
            if (_monthNumber == 2 || _monthNumber == 3 || MonthNumber == 11)
                ThisMonth.StartFrom = "Th";
            else if (_monthNumber == 4 || _monthNumber == 7)
                ThisMonth.StartFrom = "Su";
            else if (_monthNumber == 5)
                ThisMonth.StartFrom = "Tu";
            else if (_monthNumber == 6)
                ThisMonth.StartFrom = "Fr";
            else if (_monthNumber == 8)
                ThisMonth.StartFrom = "We";
            else if (_monthNumber == 9 || _monthNumber == 12)
                ThisMonth.StartFrom = "Sa";
            else
                ThisMonth.StartFrom = "Mo";
        }
        #endregion
    }
}
