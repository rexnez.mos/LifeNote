﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Business
{
    public class UserFactory
    {
        public static User Create(string _username, string _password, string _email)
        {
            return Create(Guid.NewGuid(), _username, _password, _email);
        }

        public static User Create(Guid guid, string username, string password, string email)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException("username");
            if (string.IsNullOrEmpty(password))
                throw new ArgumentNullException("password");
            if (string.IsNullOrEmpty(email))
                throw new ArgumentNullException("email");

            User _user = new User(guid)
            {
                Username = username,
                MotDePasse = password,
                Email = email
            };
            
            return _user;
        }
    }
}
