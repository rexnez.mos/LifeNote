﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Business
{
    public class ObjectiveFactory
    {
        public static Objective Create(DateTime _start, DateTime _end, string _description,
           Guid _listTaskId)
        {
            return Create(Guid.NewGuid(), _start, _end, _description, _listTaskId);
        }

        public static Objective Create(Guid guid,DateTime start, DateTime end, string description,
            Guid listTaskId)
        {
            if (string.IsNullOrEmpty(description))
                throw new ArgumentNullException("description");


            Objective _objective = new Objective(guid)
            {
                Start = start,
                End = end,
                Description = description,
                ListTaskId = listTaskId
            };

            return _objective;
        }
    }
}
