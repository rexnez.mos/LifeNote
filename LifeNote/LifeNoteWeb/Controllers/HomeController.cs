﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LifeNoteWeb.Models;
using LNW.Application;

namespace LifeNoteWeb.Controllers
{
    public class HomeController : Controller
    {
        IListTaskServices LTservices;
        public HomeController(IListTaskServices _LTservices)
        {
            LTservices = _LTservices;
        }

        public async Task<IActionResult> MyCalendar(string id)
        {
            DateTime date = DateTime.Today;
            if (id != null)
                date = DateTime.Parse(id);
            MyCalendarViewModel model = new MyCalendarViewModel(date, (LTservices.GetAllListTask()));
            return View(model);
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
