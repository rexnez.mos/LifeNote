﻿using LNW.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Business
{
    public class Objective : Entity<Guid>
    {
        public Objective()
        {

        }
        public Objective(Guid id) : base()
        {
            Id = id;
        }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Description { get; set; }
        public Guid ListTaskId { get; set; }

        public ListTask ListTask { get; set; }
    }
}
