﻿using LNW.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Business
{
    public class Note : Entity<Guid>
    {
        public Note()
        {

        }
        public Note(Guid id) : base()
        {
            Id = id;
        }
        public TimeSpan TimeStart { get; set; }
        public Nullable<TimeSpan> TimeEnd { get; set; }
        public string Description { get; set; }
        public Guid ListTaskId { get; set; }

        public ListTask ListTask { get; set; }
    }
}