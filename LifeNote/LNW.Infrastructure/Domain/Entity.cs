﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Infrastructure
{
    public abstract class Entity<TId>
    {
        public TId Id { get; protected set; }
        public int Version { get; private set; }
    }
}
