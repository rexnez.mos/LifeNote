﻿using LNW.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Business
{
   public interface IListTaskRepository : IRepository<ListTask>
    {
    }
}
