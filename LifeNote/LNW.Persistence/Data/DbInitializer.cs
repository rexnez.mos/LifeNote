﻿using LNW.Business;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LNW.Persistence.Data
{
    public class DbInitializer
    {
        public static void Initialize(TaskContext context)
        {
            context.Database.EnsureCreated();

            // Look for any users.
            if (context.Users.Any())
            {
                return;   // DB has been seeded
            }
            //Populate Users
            var users = new User[]
            {
                new User(new Guid(("79ac0a58-b993-4711-8c50-572bac89775e").ToUpper())) { Username="admin1",Email="admin1@hotmail.com",MotDePasse="admin1"},
                new User(new Guid(("69d2bbb5-e9c3-4542-9b09-9ca94c3c4756").ToUpper())) {Username="admin2",Email="admin2@hotmail.com",MotDePasse="admin2"},
                new User(new Guid(("602d6859-c030-431a-b2e6-734ba4788db3").ToUpper())) {Username="admin3",Email="admin3@hotmail.com",MotDePasse="admin3"},
            };
            foreach (User u in users)
            {
                context.Users.Add(u);
            }
            context.SaveChanges();
            //Populate ListTasks
            var listTasks = new ListTask[]
             {
                new ListTask(new Guid(("5fc88f93-55fe-4a0b-9b29-c288b1ad521e").ToUpper()))
                { DateOfList = DateTime.Today, UserId = Guid.Parse(("79ac0a58-b993-4711-8c50-572bac89775e").ToUpper())},
                new ListTask(new Guid(("328053a3-0ef7-428e-9dec-2efe331954a0").ToUpper()))
                { DateOfList = DateTime.Parse("2018-04-15"), UserId = Guid.Parse(("79ac0a58-b993-4711-8c50-572bac89775e").ToUpper()) },
                new ListTask(new Guid(("a0069cc5-90ec-423e-ae89-b86ce38fd63e").ToUpper()))
                { DateOfList = DateTime.Parse("2018-04-18"), UserId = Guid.Parse(("79ac0a58-b993-4711-8c50-572bac89775e").ToUpper()) },
                new ListTask(new Guid(("3c8eb31f-4457-45ef-82c4-d0b0a6158784").ToUpper()))
                { DateOfList = DateTime.Parse("2018-04-19"), UserId = Guid.Parse(("79ac0a58-b993-4711-8c50-572bac89775e").ToUpper()) },
                new ListTask(new Guid(("6649ddac-109b-4a34-b497-dc39812630cf").ToUpper()))
                { DateOfList = DateTime.Today, UserId = Guid.Parse(("69d2bbb5-e9c3-4542-9b09-9ca94c3c4756").ToUpper()) },
                new ListTask(new Guid(("c442ab0f-3b44-4ec1-a259-f1ea849a9076").ToUpper()))
                { DateOfList = DateTime.Parse("2018-04-22"), UserId = Guid.Parse(("69d2bbb5-e9c3-4542-9b09-9ca94c3c4756").ToUpper()) },
                new ListTask(new Guid(("b619c493-5aaf-46ed-9367-e27e92a1b782").ToUpper()))
                { DateOfList = DateTime.Parse("2018-04-23"), UserId = Guid.Parse(("69d2bbb5-e9c3-4542-9b09-9ca94c3c4756").ToUpper()) },
                new ListTask(new Guid(("9cdc5a84-4b55-4475-b6b7-4eea9699b69d").ToUpper()))
                { DateOfList = DateTime.Parse("2018-04-24"), UserId = Guid.Parse(("69d2bbb5-e9c3-4542-9b09-9ca94c3c4756").ToUpper()) },
                new ListTask(new Guid(("f9a419aa-3980-4bdf-a455-11a65d9f0519").ToUpper()))
                { DateOfList = DateTime.Today, UserId = Guid.Parse(("602d6859-c030-431a-b2e6-734ba4788db3").ToUpper()) },
                new ListTask(new Guid(("b331e123-47c0-4386-ab01-06fae2f2395a").ToUpper()))
                { DateOfList = DateTime.Parse("2018-04-30"), UserId = Guid.Parse(("602d6859-c030-431a-b2e6-734ba4788db3").ToUpper()) },
                 new ListTask(new Guid(("a7f85ddf-5e8a-43dd-b02e-5af96e186c37").ToUpper()))
                { DateOfList = DateTime.Parse("2018-05-20"), UserId = Guid.Parse(("602d6859-c030-431a-b2e6-734ba4788db3").ToUpper()) },
             };
            foreach (ListTask lt in listTasks)
            {
                context.ListTasks.Add(lt);
            }
            context.SaveChanges();
            //Populate Works
            var works = new Work[]
            {
                new Work(new Guid(("236c556a-ecfb-411b-aec1-b7ebb182e992").ToUpper()))
                { TimeStart=TimeSpan.Parse("8:00:00"),TimeEnd=TimeSpan.Parse("18:00:00"),
                    PauseStart =TimeSpan.Parse("12:30:00"),PauseEnd=TimeSpan.Parse("13:00:00"),
                    Description ="My work is funny",ListTaskId=Guid.Parse(("5fc88f93-55fe-4a0b-9b29-c288b1ad521e").ToUpper())},
                new Work(new Guid(("bb99d614-4590-40b8-981c-f4ab1d7ffc48").ToUpper()))
                { TimeStart=TimeSpan.Parse("8:00:00"),TimeEnd=TimeSpan.Parse("17:00:00"),
                    PauseStart =TimeSpan.Parse("12:00:00"),PauseEnd=TimeSpan.Parse("13:00:00"),
                    Description ="I prefer friday",ListTaskId=Guid.Parse(("328053a3-0ef7-428e-9dec-2efe331954a0").ToUpper())},
                new Work(new Guid(("3d24c37b-ad67-413d-b075-5a95214c700f").ToUpper()))
                { TimeStart=TimeSpan.Parse("8:00:00"),TimeEnd=TimeSpan.Parse("17:30:00"),
                    PauseStart =TimeSpan.Parse("12:00:00"),PauseEnd=TimeSpan.Parse("13:00:00"),
                    Description ="You should like monday",ListTaskId=Guid.Parse(("a0069cc5-90ec-423e-ae89-b86ce38fd63e").ToUpper())},
                new Work(new Guid(("87e1f840-f6bb-4fa8-afe4-8f81790da59c").ToUpper()))
                { TimeStart=TimeSpan.Parse("9:00:00"),TimeEnd=TimeSpan.Parse("14:00:00"),
                    PauseStart =TimeSpan.Parse("11:00:00"),PauseEnd=TimeSpan.Parse("11:30:00"),
                    Description ="Slavoj will win",ListTaskId=Guid.Parse(("3c8eb31f-4457-45ef-82c4-d0b0a6158784").ToUpper())}
            };
            foreach (Work w in works)
            {
                context.Works.Add(w);
            }
            context.SaveChanges();
            //Populate Notes
            var notes = new Note[]
            {
                new Note(new Guid(("d1a77405-c54a-4c56-a23e-81f047b7b19c").ToUpper()))
                {
                    TimeStart = TimeSpan.Parse("18:00:00"),TimeEnd=TimeSpan.Parse("19:00:00"), Description = "Note1 with some thing to do",
                    ListTaskId = Guid.Parse(("3c8eb31f-4457-45ef-82c4-d0b0a6158784").ToUpper())},
                new Note(new Guid(("d6ee53da-17c9-48a1-8b2f-01e6e7a14761").ToUpper()))
                {
                    TimeStart = TimeSpan.Parse("18:00:00"),TimeEnd=TimeSpan.Parse("19:00:00"), Description = "Note2 with some thing to do",
                    ListTaskId = Guid.Parse(("a0069cc5-90ec-423e-ae89-b86ce38fd63e").ToUpper())},
                new Note(new Guid(("3ff6155f-b060-4492-b278-c89954885f38").ToUpper()))
                {
                    TimeStart = TimeSpan.Parse("18:00:00"), Description = "Note3 with some thing to do,test no timeEnd",
                    ListTaskId = Guid.Parse(("328053a3-0ef7-428e-9dec-2efe331954a0").ToUpper())},
                new Note(new Guid(("1c98c409-ae56-4987-bf59-ec6296e8df2c").ToUpper()))
                {
                    TimeStart = TimeSpan.Parse("18:00:00"),TimeEnd=TimeSpan.Parse("19:00:00"), Description = "Note4 with some thing to do",
                    ListTaskId = Guid.Parse(("5fc88f93-55fe-4a0b-9b29-c288b1ad521e").ToUpper())}

            };
            foreach (Note n in notes)
            {
                context.Notes.Add(n);
            }
            context.SaveChanges();
            //Populate Objectives
            var objectives = new Objective[]
            {
                new Objective(new Guid(("db7f321b-bc6d-43aa-8619-f81d126fffe7").ToUpper()))
                {
                    Start = DateTime.Parse("2018-05-20"), End=DateTime.Parse("2018-05-30"), Description="Objective that have important things to remember",
                    ListTaskId = Guid.Parse(("a7f85ddf-5e8a-43dd-b02e-5af96e186c37").ToUpper())},
                new Objective(new Guid(("b9e9e09c-4003-40cc-8ba4-655bd03415c3").ToUpper()))
                {
                    Start = DateTime.Parse("2018-05-20"), End=DateTime.Parse("2018-05-30"), Description="Objective that have important things to remember",
                    ListTaskId = Guid.Parse(("a7f85ddf-5e8a-43dd-b02e-5af96e186c37").ToUpper())},
            };
            foreach(Objective o in objectives)
            {
                context.Objectives.Add(o);
            }
            context.SaveChanges();
        }
    }
}
