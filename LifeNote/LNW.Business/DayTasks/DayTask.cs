﻿using LNW.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace LNW.Business
{
    public class DayTask : ValueObject<DayTask>
    {
        public int DayNumber { get; set; }
        public DateTime DayDate { get; set; }
        public bool ThereIsWork { get; set; }
        public bool ThereIsNote { get; set; }
        public bool ThereIsObjective { get; set; }
        //listTask include list of Work, Note and Objective
        public List<Work> ListWork { get; set; }
        public List<Note> ListNote { get; set; }
        public List<Objective> ListObjective { get; set; }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            throw new NotImplementedException();
        }
    }
}
